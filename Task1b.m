% Compare x(t) and v(t)

% Time range for which x(t) and v(t) will be computed
t = -0.1:0.00001:0.1; % Example time vector, adjust as needed

% Initialize x(t) as a complex vector and v(t) as a real vector
x_t = zeros(size(t), 'like', 1+1i);
v_t = cos(50*pi*t);

% Compute x(t)
for k = -10:10
    if k ~= 0.5 && k ~= -0.5  % Avoid division by zero when k is 0.5 or -0.5
        x_t = x_t + ((-1)^k * exp(1i*k*100*pi*t)) / (1 - 4*k^2);
    end
end

% Scale x(t) by the constant factor
x_t = (2/pi) * x_t;

% Plot the real part of x(t) and v(t)
figure;
plot(t, real(x_t), 'b', t, v_t, 'r');
title('Comparison of x(t) and v(t)');
xlabel('t');
legend('x(t)', 'v(t)');
