syms k C

assume(k, 'integer');

% Start Voltage in V
v0 = 1;

% Resistance in Ohm
R = 30000;

% Expresion for the sum.
sum = symsum(1/(power(pi,2)*power(1-4*power(k,2),2)*power(1+R*C*100*pi*k,2)),k,1,10);

% Calculation for the Numerator
numerator = 4*power(v0,2)*sum;

%The Total Equation
totEq = sqrt(2*numerator)/(2/pi) == 0.02;

% To solve for C
C = vpasolve(totEq, C);