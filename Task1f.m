clear

syms k s t x(t) y(t) laplaceY
assume(t > 0); % time can't be negative

% Initialize variables
R = 30000; 
C = 2.4*10^(-6);
v0 = 1;

% Restricted to -8:k:8 because of processing
x(t) = (2/pi)*symsum(((-1)^k * exp(1i*k*100*pi*t)) / (1 - 4*k^2),k, -8, 8);

% Derivative of y
dy = diff(y, t);

% Laplace
left = y + R*C*dy == x;
laplaceLeft = laplace(left, t, s);

% Extract Y(s)
sub1 = subs(laplaceLeft, laplace(y, t, s), laplaceY);
sub2 = subs(sub1, y(0), 0); % Initiial Condition y(0) = 0

% Solve for Y(s) and invert Laplace
laplaceY = solve(sub2, laplaceY);
y = ilaplace(laplaceY, s, t);


% Function Plot
figure
subplot(2, 1, 1);
fplot(y, [0 0.8]);
ylabel('Voltage(V)');
ylim([0 0.8]);
legend('y(t)');
grid on;

subplot(2, 1, 2);
fplot(x, [0 0.8]);
xlabel('Time(s)');
ylabel('Voltage(V)');
legend('x(t)');
grid on;

