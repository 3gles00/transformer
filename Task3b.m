clear

% Generate the signal
M = 80;
n = 1:(M-1);
x = 2 * cos(pi * n / 20) + sin(2 * pi * n / 3) + cos(6 * pi * n / 7);

% Apply the filter
y1 = ltid(x, 4);

y2 = ltid(x, 8);

y3 = ltid(x, 16);

% Plot the original and filtered signals
plot(n, y1, 'r.');
hold on
plot(n, y2, 'g.');
hold on
plot(n, y3, 'b.');
hold off


xlabel('n');
ylabel('Amplitude');
title('Filtered Signals');
legend( 'N=4', 'N=8','N=16');
grid on;
