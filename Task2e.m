% Import the symbolic math toolbox
syms s t

%Transferfucntion H(s)
H = (40/(s^2+4*s+40));

% Declare the inputs
syms x1(t)
x1(t) = t;
syms x2(t)
x2(t) = cos(t);


syms y1(t)
y1(t) = simplify(ilaplace(H*laplace(x1(t))));

syms y2(t)
y2(t) = simplify(ilaplace(H*laplace(x2(t))));

syms diff1(t)
diff1(t) = abs(x1(t)-y1(t));

syms diff2(t)
diff2(t) = abs(x2(t)-y2(t));

timeRange = [0, 15]; % Adjust this range as needed

% Plot the functions
figure(1)
fplot(diff1(t), timeRange)
hold on
fplot(diff2(t), timeRange)
hold off
ylabel('Magnitude of Difference')
title('Steady States')
xlabel('Time (t)')
ylabel('y(t)')
grid on
legend('y1(t)', 'y2(t)|')

figure(2)
fplot(y1(t), timeRange)
hold on
fplot(y2(t), timeRange)
hold off
ylabel('Magnitude of Difference')
title('Response to different input signals')
xlabel('Time (t)')
ylabel('Amplitudes')
grid on
legend('y1(t)', 'y2(t)|')
