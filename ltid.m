% Function for LTID System/Filter
function y = ltid(x,N)
    M = length(x);
    y = zeros(1, M);
    for n = 1:M
        sum = 0;
        for k = 0:N-1
            if(n - k) > 0
                sum = sum + x(n-k);
            end
        end
        y(n) = sum/N;
    end
end