syms H(z)

% Parameters
Ns = [4, 8, 16];
M = 512; % Number of points in frequency domain

hold on;
% Frequency response calculation and plotting
for i = 1:length(Ns)
    N = Ns(i);
    % Impulse response
    h = (1/N) * ones(1, N);

    % Frequency response, by default 0≤Ω≤π
    [H, omega] = freqz(h, 1, M);

    %subplot(length(Ns), 1, i);
    plot(omega, abs(H));
    
end
hold off;

xlabel('\Omega (rad/sample)');
ylabel('|H(e^{j\Omega})|');
title(['Amplitude Response']);
grid on;